Title: Man's Reach Exceeds His Grasp
Date: 2012-04-11
Category: Creations
Summary: My first attempt at painting with acrylic.
Image: painting1.jpg
Tags: feed

The painting is called “Man’s Reach Exceeds His Grasp”. I've always wanted to try painting and thought I had a good idea, so after a couple of drawings I attempted to paint it. I eventually got it framed at Michaels. Many thanks to my friend Laura for the opportunity to do this, I couldn't have done it without her help.

![[painting1.jpg | a painting of water pouring out of a vase and into a hand, then turning to sand]]

## The Meaning

It’s hard to see in the photo, but the moment the water touches his hand it turns into sand and is taken by the slight breeze. The title is a quote from Andrea del Sarto, a poem by Robert Browning.  It is also said by Nikola Tesla’s character in my favourite movie, [The Prestige](https://www.imdb.com/title/tt0482571/).

“I, painting from myself and to myself,  
Know what I do, am unmoved by men’s blame  
Or their praise either. Somebody remarks  
Morello's outline there is wrongly traced,  
His hue mistaken; what of that? or else,  
Rightly traced and well ordered; what of that?  
Speak as they please, what does the mountain care?  
Ah, but a man’s reach should exceed his grasp,  
Or what’s a heaven for?”  
– Robert Browning from *Andrea del Sarto*

## Creation

I started with the background, trying to make it blurry and out of focus, then slowly progressed to the foreground. The hands were drawn in pencil and painted in. It was quite difficult to get the blending and shadows perfect, but I had Laura to tell me when things didn't look right. Below I am trying to figure out what a hand looks like in a mirror.

![[painting2.jpg | me looking at my own hand in the mirror as a guide]]
