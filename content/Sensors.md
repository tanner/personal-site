Title: Sensors
Date: 2022-05-24
Category: Notes
Summary: Graphs of various sensors around my house.

## 24h Graphs

These graphs are live and generated every 10 minutes, assuming the script works:

![a graph with a title, and axis labelled. they all look similar and continuously change, so I won't describe each one.](https://sensor-pics.dns.t0.vc/Solar_Power.png)

Black: power (W), green: energy (kWh)

![a graph](https://sensor-pics.dns.t0.vc/Living_Room_Air.png)

Black: PM10 (ug/m³), red: PM2.5 (ug/m³), blue: CO₂ (ppm), green: VOC

![a graph](https://sensor-pics.dns.t0.vc/Outside_Temperature.png)

Black: temperature (°C)

![a graph](https://sensor-pics.dns.t0.vc/Bedroom_Temperature.png)

Black: temperature (°C), blue: humidity (%)

![a graph](https://sensor-pics.dns.t0.vc/Nook_Temperature.png)

Black: temperature (°C), blue: humidity (%)

![a graph](https://sensor-pics.dns.t0.vc/Basement_Temperature.png)

Black: temperature (°C), blue: humidity (%)

![a graph](https://sensor-pics.dns.t0.vc/Nook_Thermostat.png)

Black: temperature (°C), red: setpoint (°C), green: state (off / running)

![a graph](https://sensor-pics.dns.t0.vc/Gas_Usage.png)

Black: total (MJ), green: delta (MJ)

![a graph](https://sensor-pics.dns.t0.vc/Water_Usage.png)

Black: total (L), green: delta (L)

![a graph](https://sensor-pics.dns.t0.vc/Living_Room_Lux.png)

Black: light (lx)

## Live Dashboard

A live interactive version can be found on this [dashboard](https://sensors.dns.t0.vc/).

You can find the [source code](https://git.tannercollin.com/tanner/sensors) on my Gitea.

## Data Capture

Most of the data is captured by two cheap RTL-SDRs (software-defined radios) that are set to listen to 433 MHz and 915 MHz radio frequencies. I use the open-source project [rtl_433](https://github.com/merbanan/rtl_433) to automatically decode the signals and forward them to an MQTT broker, which is a messaging server that services can publish and subscribe to. Other sensors run an MQTT client directly or expose their data through other means like a web interface that I poll.

The data gets collected by a central Python script that process and stores it in an InfluxDB database for "efficient" storage. The script also runs a web server that queries the database and exposes the data over an API to the dashboard at various dates and ranges. The dashboard is written in JavaScript / React using a simple chart library.

## InfluxDB Regrets

My biggest regret was using InfluxDB. It's a stupid database that I wouldn't recommend it to anyone. I ran into timezone issues with `group by time()`. It assumes the column data type is an integer if your sensor happens to send it a whole number at first and it won't let you change it. Their docs are a confusing mess. They dropped the SQL-like InfluxQL syntax for querying with a pipeline-like syntax called Flux in version 2.0. Debian's repos seem to be staying with version 1.x though. You can only delete data by time ranges, not values. It also logs every single thing to `/var/log/syslog` and there's no easy way to disable it (completely). They shut down InfluxDB cloud in Belgium and [didn't warn customers](https://community.influxdata.com/t/getting-weird-results-from-gcp-europe-west1/30615/7) before deleting all their data.

Just stick to SQLite or Postgres.
