Title: Makerspace Tours
Date: 2024-07-18
Category: Writing
Summary: A collection of makerspaces I've toured.
Tags: feed

When I travel I often try to tour Makerspaces and then share what I've learned with the one that I'm a part of, [[Protospace]]. Below you'll find links to the posts I've made on our forums about the makerspaces I've visited.

I'm interested in learning about:

- the space's history, policies, and procedures
	- how are they documented / communicated to new members
- membership demographics
- how to join
- how decisions are made and conflicts resolved
- how toxic members are dealt with

## Italy

November 2024: [LOFOIO Makerspace in Florence](https://forum.protospace.ca/t/lofoio-makerspace-in-florence/7956)

## Japan

September 2023: [Take Space in Hamamatsu](https://forum.protospace.ca/t/take-space-in-hamamatsu/4800)

September 2023: [Tokyo Hackerspace Tour](https://forum.protospace.ca/t/tokyo-hackerspace-tour/4769)

## Spain

December 2022: [Benimakers Makerspace in Valencia](https://forum.protospace.ca/t/benimakers-makerspace-in-valencia/3100)

November 2022: [Makespace Madrid Tour](https://forum.protospace.ca/t/makespace-madrid-tour/3084)

November 2022: [Fab Lab tour in Seville](https://forum.protospace.ca/t/fab-lab-tour-in-seville/3067)

## Portugal

November 2022: [Fab Farm Makerspace in Algarve](https://forum.protospace.ca/t/fab-farm-makerspace-in-algarve/3024)

November 2022: [Fab Lab Makerspace in Lisbon](https://forum.protospace.ca/t/fab-lab-makerspace-in-lisbon/2970)

November 2022: [MILL Creative Space in Lisbon](https://forum.protospace.ca/t/mill-creative-space-in-lisbon/2964)

## United States

February 2023: [SYN Shop Makerspace Las Vegas](https://forum.protospace.ca/t/syn-shop-makerspace-las-vegas/3508)

## Canada

September 2024: [Makerspace Nanaimo](https://forum.protospace.ca/t/makerspace-nanaimo-tour/7576)

October 2022: [Makerspace Tour in Turner Valley](https://forum.protospace.ca/t/makerspace-tour-in-turner-valley/2795)

June 2022: [Makerspace in Prince George, BC](https://forum.protospace.ca/t/makerspace-in-prince-george-bc/2112)

## Honorable Mentions

Makerspaces I've visited, but didn't do a write-up tour for:

- Protospace in Calgary
- Archloft in Calgary
- Fuse33 in Calgary
- Fab Lab Barcelona
- FabCafe Barcelona
- TMDC Barcelona
- C2 Space Kyoto
