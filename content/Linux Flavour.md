Title: Choosing a Linux Flavour
Date: 2020-10-31
Category: Writing
Summary: A recommendation on which flavour of Linux to run.
Image: distro1.png
Wide: true
Tags: feed

[TOC]

People often ask me which flavour of Linux they should install. In summary, choose Ubuntu if it's your first time. Once you are comfortable, install Debian the next time you need to install Linux.

I run Debian on my computers and servers.

## Linux Distributions

<span class="aside">(Interjection: it's technically called GNU/Linux)</span>

When people refer to the "flavour of Linux" they are talking about a Linux distribution (distro). It mostly describes what software is distributed in its software repository.

"A typical Linux distribution comprises a Linux kernel, GNU tools and libraries, additional software, documentation, a window system, a window manager, and a desktop environment." [Wikipedia]

The major Linux distros are practically all the same. If you master one it's easy to pick up the others. The main differences you'll run into are which tools you use to install new software, and the desktop environment, which is what all the windows and buttons look like.

![[distro1.png]]

I recommend two Linux distros, Debian and Ubuntu. Ubuntu is based off of Debian, so they are very similar. 

## Pros of Debian

Debian is one of the oldest distros and many other distros are based off it.  You can see a timeline visualization of all its derivatives here:

<https://upload.wikimedia.org/wikipedia/commons/1/1b/Linux_Distribution_Timeline.svg>

This image is what originally convinced me to use Debian. Scroll down until you see it and zoom out so you grasp how many derivatives it has.

Debian is also non-commercial and requires that all software in its main repository is free and open source. This is important because that grants you the right to study, change, and distribute the software and source code to anyone and for any purpose. They also follow a strong social contract you can see here:

<https://www.debian.org/social_contract>

It's also a very stable Linux distro since they freeze all software features on each release. This makes it great for servers because nothing will break when it updates.

The main Raspberry Pi distro is nearly identical to Debian, so you'll also gain familiarity with it.

## Cons of Debian

Since Debian requires all its software to be free and open source, proprietary hardware drivers aren't included in its main repo. This can make installing Debian difficult if your hardware requires proprietary drivers. You'll need to use an installation image found here:

<https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/>

The fact that Debian freezes software features can also mean that your software gets old until the next Debian release. If you want versions that are bleeding edge, you'll need to use Debian Unstable as described here:

<https://wiki.debian.org/DebianUnstable#Installation>

Don't be fooled by the name "unstable". I use it for my personal computers and it runs fine.

## Pros of Ubuntu

Ubuntu is incredibly easy to install. You can also try it out before deciding to install it. The distro pretty much just works on what ever hardware you have.

It's very beginner friendly because it's so popular. Any problem you search for will reveal dozens of threads with people solving the same problem.

## Cons of Ubuntu

Unfortunately Ubuntu is developed by a commercial company, Canonical. The company's interests come first, before the users' and they have a track record of betraying their users' trust and privacy. 

Years ago Ubuntu had a feature enabled by default that would send your desktop searches to Amazon so they could suggest products for you to buy:

<https://www.pcworld.com/article/2840401/ubuntus-unity-8-desktop-removes-the-amazon-search-spyware.html>

Currently whenever you remote login to your Ubuntu machine, it phones home to Canonical and they collect info about your system:

<https://ubuntu.com/legal/motd>

While these reasons are fairly minor, they are quite frowned upon in the Linux community and are reason enough to switch to Debian once you are comfortable with using Linux.

[Wikipedia]: https://en.wikipedia.org/wiki/Linux_distribution
