Title: Hand of Ozymandias
Date: 2012-03-23
Category: Creations
Summary: A withered hand I welded out of scrap metal.
Image: hand1.jpg
Tags: feed

I was visiting my cousins in Radium, BC and decided to learn stick welding at their shop. I wanted to create a sculpture, so with pieces of scrap metal I welded together this hand. The beads are far from perfect. Working with small pieces of rusted metal made it difficult.

![[hand1.jpg | a rusted hand welded together out of scrap square stock metal tubing]]

## The Name

One of my favourite poems is [Ozymandias](https://en.wikipedia.org/wiki/Ozymandias) by Percy Bysshe Shelley. It's about the inevitable complete decline of all rulers and the empires they build, however mighty in their time. This is the hand of Ozymandias sticking out from the sand, grasping for life after he has been reduced to dust.

## Construction

I eyeballed the joint angles and my cousin cut them to spec with an angle grinder. It was made in a machine shop with no real planning done ahead of time.  In between welds, I used my own hand as a reference. Below is a picture of me adding a bead to it.

![[hand2.jpg | me welding the hand causing a very bright white light that washes out the photo]]
