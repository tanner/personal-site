Title: Fake Dog for Home Security
Date: 2022-06-27
Category: Creations
Summary: Fake dog barking for home security while on vacation.
Image: fake-dog.jpg
Tags: feed

I set up a fake dog that barks if my surveillance cameras are triggered while I'm out of town on vacation. It's a pair of computer speakers plugged into a Raspberry Pi, which is an inexpensive single-board computer. One speaker faces the front door and the other faces the side door.

When the front door camera is triggered my surveillance camera system sends a message to the Raspberry Pi. A simple program plays an audio clip of a big dog barking through the side speaker and then the front speaker. The change in speakers simulates a dog moving towards the front door. The opposite happens if the side door camera is triggered.

You can find the [source code](https://git.tannercollin.com/tanner/woof) on my Gitea.

![[fake-dog.jpg | a speaker connected to a circuit board on my hardwood floor]]

## Technical Details
My surveillance cameras sit on a separate network without internet access and their RTSP streams are consumed by the Blue Iris NVR software running on a dedicated Windows box. When an object is detected moving through a defined area for certain cameras, Blue Iris is configured to send an MQTT message to the `iot/cameras` topic via the Mosquitto broker running on my media server.

A Python script kept alive by Supervisor runs on the Raspberry Pi and listens to the topic using the  `asyncio-mqtt` module. It receives and tries to decode a JSON message like `{"serial": "SE-N-ZoneB"}`. If the camera's serial is found in a dict at the top of the script, the corresponding audio file is played using Pygame. Controlling which speaker barks is done by muting the left or right channel in the stereo audio file.

![[fake-dog2.png | a console log output reporting the camera has activated]]
## Future Improvements
The dog has a lot of false positives from the cameras being triggered by car headlights or small animals<span class="aside"> (like a real dog)</span>. This isn't a big deal since no one is home to hear it bark and it isn't loud enough for my neighbours to hear (I've asked). I would rather have a false positive than a false negative.

A huge improvement would be to combine the camera data with a physical trigger. A break beam sensor mounted on stair railings or an accelerometer mounted under the steps would work for this. But it would be annoying to mount, route power to, and harden for harsh Calgary winters. Perhaps the accelerometers are sensitive enough to mount inside, close to the entrances.

Update: a reader emailed me mentioning that he has a similar set up that's triggered by a HFS-DC06 radar sensor mounted inside his house. This module seems like a good option for me because it can sense through an external wall.

## Efficacy
According to [former burglars](https://news.t0.vc/TRMA), barking dogs and security cameras are some of the best deterrents to home invasions. The dog sounds fake to me, but I know it isn't real. I'm hoping that a rushing burglar won't spend time pondering if the barking is from an elaborate speaker setup and will just move on to a different house.

I previously caught a prowler who went into my backyard and tested my garage door handle to see if it was locked. He then broke into my neighbour's truck and garage after. He gets to have his face on my website:

![[fake-dog3.jpg | a would-be burglar testing my garage door handle on the left, a close-up of his face on the right]]

The extra peace of mind while I'm away is worth the evening it took to set up. It was very easy to make because I reused the code I wrote for [[Protospace]]'s PA system doorbell that you can find on [GitHub](https://github.com/protospace/doorbell). It operates similarly by playing an audio file based on which 433 MHz doorbell is pressed.
