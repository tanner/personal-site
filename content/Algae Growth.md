Title: Algae Growth
Date: 2023-03-11
Category: Notes
Summary: Prevent hydroponic algae from growing by blocking light to any nutrient solution.

Algae will grow anywhere light is able to touch hydroponic nutrient solution. This won't be a problem if the solution is regularly flushed. In Kratky systems the algae will compete for resources with your plants and grow exponentially.

The solution is to prevent light from reaching the solution by using dark containers and covering exposed parts with and opaque material such as aluminium foil.
