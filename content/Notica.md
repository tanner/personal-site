Title: Notica
Date: 2022-05-17
Category: Projects
Summary: Send browser notifications from your terminal. No installation. No registration.
Image: notica1.jpg
Tags: feed

[Notica](https://notica.us) allows you to send browser notifications from your terminal to know when a slow command has finished running. It doesn't require installing anything or registering an account. It also works over ssh unlike `notify-send`.

You can find the [source code](https://github.com/tannercollin/Notica) on Github.

![[notica1.png]]

I do most of my work on remote servers over ssh. When running a slow command (like `apt install`) I'll distract myself by browsing sites like my other project [[QotNews]]. The command will finish running, but I'll still be wasting time reading news articles. Notica helps me stay on track by alerting me when the command finishes.

Example uses of Notica would be:

```
$ sudo apt install freecad ; notica finished installing

$ rsync -av backup/ myserver:/mnt/backup/ ; notica done backup
```
