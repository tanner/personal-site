Title: Garage Door Opener Hack
Date: 2021-12-26
Category: Creations
Summary: Hacking my garage door opener to work over Wifi.
Image: garage3.jpg
Tags: feed

In the quest to automate as much of my house as possible, I thought it would be useful to be able to remotely control my garage door from my home automation system. If I suspected that I forgot to close it while leaving, I could check in my security cameras and then close it from anywhere. It's nice having this peace of mind, even if it almost never happens.

Instead of reverse engineering the wireless protocol, cracking the encryption, and sending my own commands, I figured it would be much easier to hack the hardware. I pried open a spare remote to find that it contained a basic PCB with simple tactile switches.

![[garage1.jpg | the six parts of the spare remote on my desk: metal clip, plastic buttons, battery, PCB, and two halves of the case]]

My plan was to solder an Arduino controlled relay in parallel with the button that toggled the garage door. I would also power the remote from the 3 V pin. I soldered four wires on to the appropriate pins.

![[garage2.jpg | a closeup of the green PCB with two wires soldered to the battery terminals and two to the button pins]]

I drilled a hole in the plastic case and routed the wires through it while reassembling the remote. It's connected to an Adafruit ESP8266 Arduino with a relay module shield.

The Arduino joins my isolated home automation Wifi network and connects over MQTT, a simple messaging protocol, to my automation server. When it receives a command over MQTT it toggles the relay on for a quarter of a second and then off, simulating a button press.

![[garage3.jpg | the completed system: an arduino with a white relay and wires leading to the reassembled garage door remote]]

Update: this system has been flawless for two months now. It's worked every time I've tried to toggle the door and has never opened by mistake.
