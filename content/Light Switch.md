Title: Remote Control Light Switch
Date: 2014-10-09
Category: Creations
Summary: A device to toggle my lights remotely.
Image: light1.jpg
Tags: feed

I wanted the ability to toggle my bedroom light remotely for convenience. I designed a circuit that allows me to control my light with any device that can load a webpage.

I still wanted to be able to control the light manually, so I bought a metallic face-plate and turned it into a capacitive touch sensor. The slightest touch anywhere on the plate is enough to toggle the light. I had to electrically isolate the metal screws from it because they screw into a grounded switch box.

![[light1.jpg | my custom light switch, a Raspberry Pi computer, and an old iPhone]]

## Function

I have a Raspberry Pi ($35 computer) on my home network that runs a web server.  When you connect to it in your web browser, a page loads with buttons to turn the light on or off. When you press a button, the server executes a command that sends a message over Bluetooth to the light switch. The Bluetooth module in the wall receives this message and forwards it to the microcontroller, which processes it and toggles the relay. The whole circuit is also powered from mains by an AC-DC converter.

This entire process happens quicker than half a second, so it feels instant.

![[light2.jpg | the front side which has several electrical components]]

<span class="aside">(Black stuff's liquid electrical tape)</span>

![[light3.jpg | the back side which has wires soldered to connect all the components]]
