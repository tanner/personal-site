Title: Wine Crate Coffee Table
Date: 2018-09-12
Category: Creations
Summary: A coffee table made out of wooden wine creates.
Image: wine3.jpg
Tags: feed

My close friend Odai saw a simple coffee table design online that was built out of four wooden wine crates. They are quite cheap and available at any hardware store. We each wanted to make one so went and bought eight crates and some plywood to use as a base.

We went to my local makerspace, [[Protospace]], to build them in the wood shop. We thought it would be a quick job only taking a few hours, but it turned out to be a twelve hour job over a couple of days. At least we were in good company!

![[wine1.jpg | the table before staining. four box-like wine crates are on their sides and joined together in a spiral. a messy wood shop is in the background]]

![[wine2.jpg | the two tables after staining. one face is centred and the contrast between the dark stain and light wood grain shows.]]

The wine crates were glued and then brad-nailed together. Extra wood was added under the thin top strips for support. After the glue dried, the brad nails were painstakingly removed because the ones we used were too long and stuck out.

After being attached to the base, the entire table was sanded and then stained.  We let it dry overnight and returned the following day to add a quick layer of varnish.

![[wine3.jpg | the table in my living room with blankets inside the wine crates and a plant pot in the centre growing a bonsai tree and a succulent]]
