Title: Spaceport
Date: 2022-05-16
Category: Projects
Summary: Member portal for Calgary Protospace. It tracks dues, courses, training, access cards, and more.
Image: spaceport1.jpg
Tags: feed

[Spaceport](https://my.protospace.ca) is the member portal that I wrote for [[Protospace]], a makerspace that I frequent in Calgary. It is by far my largest project and the one I've spent the most time on. It has a database of all our members and tracks their transactions like dues and training fees. It allows members to sign up for classes and our instructors to teach courses. It also manages the access cards that members use to get into the building.

You can find the [source code](https://github.com/Protospace/spaceport) on Github.

![[spaceport1.png | a screenshot of spaceport's home page. a photo of me to the left, below that my latest training and transactions. links and stats related to Protospace to the right.]]

Spaceport is tightly coupled to Protospace and has many integrations:

- Syncs credentials to our AD controller for computer logins
- Manages AD group membership to restrict machine use
- Syncs credentials to our Discourse forum [Spacebar](https://forum.protospace.ca)
- Manages Discourse groups to restrict topic access
- Syncs credentials to our [MediaWiki](https://wiki.protospace.ca)
- Processes PayPal payment notifications
- Syncs valid member cards to our door controller Airlock
- Tracks training certifications for our tool lockouts
- Emails interested members when a class is scheduled
- Shows who's connected to our [Minecraft server](http://games.protospace.ca:8123/?worldname=world&mapname=flat&zoom=3&x=74&y=64&z=354)
- Tracks who's logged into our laser cutter and CNC router
- Tracks and bills for laser cutting time
- Shows if the building alarm is armed or disarmed
- Displays charts of various environmental sensors
- Displays a photo of our garden

![[spaceport2.png | a list of all the classes. they are group by course and have colourful tags to make them easier to find.]]

As of writing this there's 234 current Protospace members and 1408 historical or inactive memberships that it manages. Data is stored in a 49 MB SQLite database which makes it easy to back up or sync with my development server. The back end is written in Django / Python and the front end is React / JavaScript with Semantic UI for the graphics.

Site data is automatically compressed and [[Backup Strategy | backed up]] daily by two members. The software is free and open-source and can be set up by reading the documentation. Protospace directors also have admin access to the server's host in case something happens to me.
