Title: War with Aphids and Mould
Date: 2022-07-29
Category: Notes
Summary: I lost a war against aphids and mould.
Wide: true

[TOC]

My [[Hydroponics#Home Round 3 | hydroponic strawberries]] suffered from aphid and mould infestations. This took me by surprise because one of the advantages to growing plants hydroponically is that you don't get pests from growing indoors.

The aphids and mould probably came from the strawberry starters I bought from a plant store. I didn't bother rinsing the leaves when washing away the dirt. If one aphid egg or mould spore was still on the plants after transplanting into my hydroponics system, that would be enough to infect them all.

Lesson learned: Wash starters thoroughly with soap (including leaves) to prevent aphids or grow from seeds. Don't let plants grow so dense that airflow is blocked.

## Aphids
While maintaining the garden, I noticed small white flecks which I just assumed were somehow part of the plant. I later noticed a sticky substance on some of the leaves which I thought might have been water that got mixed with something. I was in total denial until I turned over a leaf and saw a bunch of aphids crawling around.

The white flecks turned out to be eggs the aphids were laying. The sticky substance was honeydew, a sugary liquid that aphids secrete.

![[hydro-war1.jpg | underside of a green leaf in a plastic bag. several dozen aphids can be seen on it.]]

I bought a bottle of Safer's insecticidal soap to use against them. I have a walk-in shower in the downstairs basement bathroom beside the grow room, so I moved all the plants into there. I then sprayed the leaves as much as I could, top and bottom. I let it drain for a while and then rinsed thoroughly with water.

![[hydro-war2.jpg | six plants sitting in a tote container lid suspended with their white roots dangling down. all inside a shower.]]

Three days later I noticed the aphids were still alive. So I bought five more bottles of aphid spray and emptied them into a bucket. I then took each plant out of the system and dunked them in the bucket, covering all leaves and the net cup. I pushed the plants in and out of the bucket forcefully, to make sure all the aphids were flushed off. I then rinsed the plants in the shower thoroughly like before.

![[hydro-war3.jpg | a strawberry plant being submerged in a white bucket of aphid spray. its roots are dangling off the side of the bucket.]]

Three days later I did this all over again, just to make sure they were gone. This seemed to work but I noticed the aphids returned a couple months later. It was one of the reasons I decided to end this round of growth experiment.

## Mould
A few weeks after the aphid dunking I noticed some kind of mould or powdery mildew on the strawberry crowns, right in the centre of the plants.

![[hydro-war4.jpg | up close view of a strawberry plant crown covered in fuzzy grey mould spores]]

I assumed this was because I let the plants' leaves get too dense and they were blocking airflow to the crown. The airstone bubbles air into the water reservoir and the only place the humid air can escape is through the net cups and strawberry crown.

I rinsed all the plants off like I did with the aphids and set up an extra fan to try and increase airflow. This worked well for a couple months until I noticed it had returned and started making the plants unhealthy. This was the other reason I decided to end the experiment.
