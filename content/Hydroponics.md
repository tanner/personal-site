Title: Hydroponics
Date: 2022-07-29
Category: Writing
Summary: My experiments growing food with hydroponics.
Wide: true
Tags: feed

[TOC]

Hydroponics is a method of growing plants without soil by delivering nutrients via water. In theory, it offers many advantages over soil: no dirt, no weeding or pests<span class="aside"> (therefore no herbicides or pesticides)</span>, no mould, no root rot, year-round growing indoors, less space required, 90% less water usage, and 30-50% faster growth. Downsides include electricity consumption if using grow lights or water pumps and the need to hand-pollinate flowers from the lack of bees.

## Home Experiments
I'll outline my hydroponics experiments here, describing what I tried, what worked, what didn't, and lessons I learned. I have a small storage room in my basement that I use for my [[Secret Garden]]. It's about 5' x 10' large with unpainted drywall and a concrete floor.

### Home: Round 1
Started: 2022-02-21, ended: 2022-03-04.

[[Hydroponics Log 1 | Grow Log]]

I tried germinating iceberg lettuce, romaine lettuce, butterhead lettuce, arugula, cress, and spinach seeds inside a germination dome. I used rockwool starter plugs soaked in growth solution. Everything except spinach sprouted. I placed a grow light on top of the dome set to minimum. Apparently this wasn't enough light since the sprouts got really leggy so I blasted them with light instead. An hour later they got soft and fell over from their own weight. I decided to restart.

Lessons learned: what leggy sprouts look like and to use enough light.

![[hydroponics1.jpg | twelve rockwool starter cubes with thin, leggy seedlings in a mess]]

### Home: Round 2
Started: 2022-03-05, ended: 2022-04-14.

[[Hydroponics Log 2 | Grow Log]]

I tried germinating the same seeds as last time but with more light. Everything except spinach sprouted again and I transplanted them to a large tote. They sat in net cups put into 4" holes drilled into pink hard foam insulation called Foamular. Arugula and cress grew roots much faster than the lettuces and drank the water level down below the rockwool. This caused the lettuces to dry out, stunting their growth, making the problem worse. Everything eventually grew and I was able to harvest a salad that tasted bad. I ended the experiment because the lettuces were so stunted.

Lessons learned:

- Make sure all the roots make it into the water.
- Start cress and arugula later, they grow fast.
- Don't rely on clay balls to wick water up to the rockwool (it won't).
- Don't bother germinating in a dome, just do it straight from the net cups.
- Don't use Foamular to hold the net cups. It requires a higher water level, doesn't index for time-lapse photography, and drops pink foam bits into the water.

<span class="aside">(Top: lettuces, bottom: arugula, cress, arugula)</span>

![[hydroponics2.jpg | six plants sitting in holes cut out of pink foam board]]

### Home: Round 3
Started: 2022-04-14, ended: 2022-07-23.

[[Hydroponics Log 3 | Grow Log]]

I bought six strawberry starters and rinsed all the dirt off of the roots. Things were going well until I noticed they were covered in aphids. I sprayed them with aphid spray but it wasn't enough, so I went to [[Hydroponics Aphid War | war with aphids]]. I ordered five bottles of aphid spray, drained it into a bucket, and dunked each plant. I let the plants grow too big (by pruning buds) and the leaves started blocking airflow to the crowns. The crowns became humid and started growing mould which I had to rinse off. I used a small paintbrush to pollinate the flowers by hand. After returning from vacation, one strawberry was ripe for me to eat. But the aphids and mould had returned so I ended the experiment.

Lessons learned:

- You can be fairly rough when washing dirt off of starters' roots.
- You can get pests in hydroponics if you aren't careful.
- Wash starters thoroughly with soap (including leaves) to prevent aphids or grow from seeds.
- Boil all equipment before using it to sanitize.
- Don't let plants grow so much that the leaves block airflow.
- Wash hands before hydro gardening.
- Hand-pollinating flowers is tedious.
- Strawberries drop tons of pedals all over the floor.

![[hydroponics3.jpg | six strawberry plants with crazy dark green leaves creating a thick bush. there are pink flowers on top under two grow lights.]]
