Title: Helios Alpha
Date: 2024-02-21
Category: Notes
Summary: Design of my previous Kratky method hydroponics system.
Wide: true

Helios Alpha is a hydroponics system that can grow up to six plants. It's designed around a single 102 L plastic tote. It holds enough water to harvest lettuce once before refilling. After the initial setup, the system can be ignored for weeks.

![[heliosalpha1.jpg | the hydroponics system from two angles. a black tub with yellow lid, covered in tin foil with six holes for plants. grow lights above suspended by metal shelving.]]

Here's directions on how you can make your own:

## Parts
- [HDX 102L Stackable Strong Storage Tote Bin](https://www.homedepot.ca/product/hdx-102l-stackable-strong-storage-tote-bin-plastic-organizer-box-black-base-yellow-snap-on-lid/1000706729)
- [HDX 36-inch W x 54-inch H x 14-inch D 4-Shelf](https://www.homedepot.ca/product/hdx-36-inch-w-x-54-inch-h-x-14-inch-d-4-shelf-steel-wire-shelving-unit-with-adjustable-shelves-in-chrome/1000790396)
- 2 x [TS-1000 Grow Lights](https://www.amazon.ca/s?k=TS-1000+grow+light)
- 6 x 3" hydroponic net cups
- Medium to large fan
- Aluminium foil
- Wall outlet timer
- Power strip
- Zip ties

### Tools
A hole saw drill bit with the same diameter of your net cups is recommended.

### Assembly
Assemble the metal shelf with one shelf at the very top and another only a foot below it as seen in the photo. Only one shelf is used, but two are needed for stability.

Hang the grow lights with the included hangers off the lower shelf. The height can be adjusted later. Plug the lights into a power strip and secure all cords to the lower shelf.

Drill six holes in the plastic lid for where you want the plant to go. Bias the holes towards the edges so the plants are more spread out for air flow.

Cover the lid with two strips aluminium foil to prevent light from passing into the nutrient solution. This prevents [[Algae Growth |algae growth]] which will kill your plants and reflect the light back up to the leaves. Tape the aluminium foil to the lid and then cut holes in it the size of the net cups. Scrunch excess foil down into the hole.

Insert the six net cups into the six holes. Place the lid back on the tote and position it under the grow lights.

Point a fan towards where the plants will grow. [[Airflow |Airflow is important]] indoors to enable water transpiration so the leaves don't get mineral deficiencies. Plug the fan in but keep it off until the starters have grown their "true leaves".

Plug the power strip into the outlet timer and adjust it for 16 hours on, 8 hours off. The specific timing will depend on the plant.
