Title: Grow Log, Home: Round 2
Date: 2022-07-29
Category: Notes
Summary: Grow log for my home hydroponics setup, round 2.
Wide: true

2022-03-05: Started germinating  
2022-03-07: Just barely noticed germination  
2022-03-08: Noticed all leaves except spinach again  
2022-03-10: Transplanted into tote  
2022-03-12: measured pH: 6.0  
2022-03-20: thinned out weak plants  
2022-03-25: lettuces have no roots below net pot. others have lots of roots in the water. some leaves have a weird film on them, some have white stuff.  
2022-03-28: Three lettuces wool dry on top. No roots below net cup. Roots are growing a bit out the sides. Dropped wool to bottom of net cups, topped water up to 1 cm above bottom of net cup. This might harm the other plants? The lettuce roots grew through a lot of the clay balls but not enough to reach the water.  
- I assumed the clay balls would wick up enough water that it wouldn't matter.  
2022-03-30: EC measured 1.1. trimmed oldest leaves, lettuce roots now good in water  
2022-04-01: pH measured 6.7. brown tips on lettuce.  
2022-04-06: Harvested first salad, arugula and cress taste horrible like paint thinner. Lettuce was good.  
2022-04-14: Terminated.  


## Germination
- index is top left in tray
- rinsed all equipment with water
- 1 L growth solution prepared
	  - 1 L tap water
	  - 1.3 mL plant food
	  - 0.5 mL pH Down for 5.5 pH
- forgot to shake plant food
- 12 mineral wool cubes each soaked for 10 seconds
- three seeds of each, except five for arugula and cress in X pattern
- hole was opened with a broken toothpick
- seeds tamped down the hole with the toothpick
- remaining solution added to the bottom of the tray

### Result
(columns left to right)

- iceberg lettuce LT541
- arugula MS483
- romaine lettuce LT470
- cress MS495
- butterhead lettuce LT458
- spinach SP705


## Growth
- used 102 L HDX tough totes
	  - sides bow out quite a bit, try duct taping
- 100 L growth solution prepared
	  - 100 L tap water (estimated)
	  - 117 mL plant food (needs more?)
	  - 10 mL pH down to 6.0
- cut six holes into Foamular
- transplanted sprouts into net cups
- layer of clay balls under and around sides of starter plug
- filled tote pretty much to brim
- tote sides bowed out quite a bit. maybe it's 110-120 L full of water?

### Layout:
(reading order)

- iceberg lettuce LT541
- romaine lettuce LT470
- butterhead lettuce LT458
- arugula MS483
- cress MS495
- arugula MS483
