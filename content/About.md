Title: About
Date: 2022-07-23
Category: Notes
Summary: About me and my website.
Wide: true

## Me
I was born in the early '90s and spent a lot of time growing up playing with computers and electricity. It eventually became my passion and lead to me getting a degree in electrical engineering with a minor in computer engineering. I then got into makerspaces and the maker movement: like-minded people getting together to build things for fun. Most of what I build is software because I can work on it from anywhere. When I build physical things they usually involve electricity as a means to allow software to interact with the world. I work in the home automation field and have spent a lot of time automating my own home.

### Uses
I do my computing on a ThinkPad X1 Carbon laptop running Debian GNU/Linux with GNOME. Most of my work is done over ssh because it allows me to pause or move to my desktop quickly and there's less risk of losing data. I edit text with Vim in Byobu (tmux) terminal sessions. I browse the web with Librewolf, a privacy fork of Firefox. I mainly communicate via Telegram Messenger or email.

I don't like tweaking or configuring settings so I try to leave things default unless something really annoys me or it improves my workflow greatly. It's easy to sink an infinite amount of time into optimizing your workflow and then die having made nothing.

## Website
There's two versions of this website, a main version at <https://tanner.vc> and a lite version at <https://t0.vc>. The reason is because I found myself continually removing features from the main version for sport and to satisfy my millennial craving for brutalist design. I was already running several [[t0 Services | services]] on t0.vc subdomains but had nothing on the main domain. So it's the perfect use for it and I can experiment to see how brutalist I can make it.

### Colophon
I use the static site generator Pelican to build the websites from a folder of markdown documents which I edit with Obsidian in Vim mode. This makes writing content feel like taking notes, since media and internal links are taken care of. The two versions are simply different themes loaded by different Pelican configs. The output is uploaded to my host via rsync and served by Nginx.

You can find the [source code](https://git.tannercollin.com/tanner/personal-site) on my Gitea.
