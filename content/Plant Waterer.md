Title: Automatic Plant Waterer
Date: 2014-06-05
Category: Creations
Summary: A device that automatically waters plants.
Image: waterer2.jpg
Tags: feed

One day I decided watering my one plant was too much work, so I automated it.  It's also great for when I'm on vacation. The plant is a year old now and doesn't look as good as it used to (kinda like you). So this machine is like its life support.

<span class="aside">(Update: this plant died long ago)</span>

![[waterer1.jpg | the device and pump on a 2L pop bottle with a tube running to a flowerpot]]

## First Attempt

The design was very simple and soldered together on perf board. A microcontroller turns the pump on for 20 seconds, then waits 24 hours and restarts. The pump ran way too fast so it was slowed down to 10% power.

This design suffered from a fatal problem. After running, there was a chance that the tube would stay full of fluid. If the water level in the pop bottle was too high, it could siphon out. I woke up with a flower pot overflowing with water a couple of times.

![[waterer2.jpg | a new version feeding into a different plant]]

## Second Attempt

I liked the idea so much that I made a second iteration. This one used a custom printed circuit board with a lot more features. The pumping duration could be adjusted with a screwdriver. This was useful as the plant (now a [Ming aralia](https://en.wikipedia.org/wiki/Polyscias_fruticosa)) grew.

Another feature was the ability to run the pump backwards. This completely eliminated the siphoning problem from before. After pumping for a set duration, it would run backwards until the tube was cleared of water.

<span class="aside">(Also dead)</span>

![[waterer3.jpg | the new version beside a big Ming aralia plant with bushy drooping leaves and skinny stems]]
