Datasheet:
https://www.espressif.com/sites/default/files/documentation/0a-esp8266ex_datasheet_en.pdf
Tech Reference:
https://www.espressif.com/sites/default/files/documentation/esp8266-technical_reference_en.pdf
PCB Design Guidelines:
https://www.espressif.com/sites/default/files/documentation/esp8266_hardware_design_guidelines_en.pdf

Pins
====

Multiplexing analog input:
https://internetofhomethings.com/homethings/?p=530

Digital:
- has hysterisis
- Vil = 0.25 * Vin
- Vih = 0.75 * Vin

ADC:
- only 1 analog pin, A0
- 10 bits, 0-1023
- only supports 0.2 - 1.2 V, unless there's on-board voltage divider


WeMos D1 Mini
=============

- has on board 3V3 ADC divider
