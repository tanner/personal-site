Title: Grow Log, Home: Round 3
Date: 2022-07-29
Category: Notes
Summary: Grow log for my home hydroponics setup, round 3.
Wide: true

2022-04-14: Planted strawberry starters.  
2022-04-21: Observed fresh white roots shooting out horizontally.  
2022-04-23: Cut flower buds off BCDEF.  
2022-04-24: Measured EC at 1.2 and pH at 6.8. Added 20 mL of pH down.  
2022-04-25: Measured pH at 6.2.  
2022-04-28: Cut a bud off of E.  
2022-05-01: Cut buds off BCDF.  
- too many buds, no longer tracking  
2022-05-05: Noticed some young leaves are brownish. Turned grow lights down to 50%.  
2022-05-22: Noticed sticky leaves and white flecks. There's tons of aphids.  
2022-05-24: Sprayed all plants liberally with aphid spray and rinsed off after.  
2022-05-27: Noticed aphids are still alive. Ordered more spray.  
2022-06-01: Dunked plants in aphid spray, rinsed off with water.  
2022-06-04: Dunked plants in aphid spray again, dunked in freshwater, transferred plants back. Measured pH at 4.0. Added more water to bring it up to 5.6. EC at 0.8  
2022-06-19: Swapped nutes for General Hydroponics. (2nd preparation below)  
2022-06-20: Noticed some sort of powdery mildew at base of leaves on a couple plants. Rinsed all with water and removed dead leaves.  
2022-06-??: Started hand-pollinating with a q-tip. Breaks cotton up.   
2022-07-05: Measured pH at 4.4, EC at 2.1. Added 5 gal water to bring it up to pH 5.5, EC at 1.4. Tons of flowers, pollinated with small brush.  
2022-07-19: Measured ph at 3.8, EC at 1.8. Added 5 gal water. Measure later.  
2022-07-20: Ate first strawberry. It was delicious.  
2022-07-23: Way too much mould, aphids, and dead leaves. Terminated.  

## Preparation
- boiled clay balls to sterilize
- rinsed dirt off 6 strawberry starters
	  - three normal
	  - two pulled thick roots out and broke away rest
	  - one washed with Method hand soap
- 90 L-ish growth solution prepared
	  - up to bottom of net cups
	  - 100 ml plant food to 1.2 EC from 0.3 EC tap water
	  - 20 ml pH down to 6.0 pH from 6.5 pH with food
- holes cut in net cups, roots thread through
- remaining space filled with clay balls
- tote lid covered with aluminum foil
- air stone added to one side
- 16 hours of light

## 2nd Preparation
- drained tote with 5 gal buckets, washed out with water
- transferred 20 gal with 4 x 5 gal bucket trips
- measured plain water EC at 0.2
- added 132 mL Bloom, 94 mL Micro, 94 mL Gro
- measured 1.7 EC, 6.5 pH
	  - difference of 1.5 EC
- added 15 mL pH down to 6.2 pH
- ** stole 5 gal for Protospace
