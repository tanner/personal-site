Title: t-zero Services
Date: 2022-05-27
Category: Writing
Summary: A list of minimal microservices on my t0.vc domain.
Image: t0services1.svg
Wide: true
Tags: feed

The t-zero Services are a collection of minimalist microservices that I host on my t0.vc domain. The letter "t" meaning me, and "0" meaning small. They're all meant to do exactly one thing reliably and stay online for as long as I can host them.

## t0.vc
The smallest t-zero is the main domain itself at [t0.vc](https://t0.vc) and it serves as a lite version of my personal website, which you are reading this on. It fits under 1000 bytes in size without being boring to read because it sacrifices everything else for pure content. Including closing HTML tags.

The rest of the t-zero services are hosted on its subdomains.

![[t0services1.svg]]

## t0txt
The second t-zero I wrote was [[t0txt]], a pastebin that is compatible with the command line and `curl`. This allows me to very easily pipe text data into it and immediately get a URL that I can share. I copied the idea from [sprunge.us](http://sprunge.us/) which kept going down because he'd forget to pay his Google Cloud bill.

## t0pic
I figured that since I have a pastebin, I might as well make an image host since Imgur now sucks. It supports web and command line upload and pasting directly into the web page. I don't advertise it because public image hosts usually end up abused. However, you can easily find it if you follow the naming pattern.

## t0url
Next I created a URL shortener called t0url because I wanted to transfer links between devices quickly. Other shorteners use longer IDs but since this is my own, I can get away with only four capital letters which makes them easy to type and remember. I also don't advertise it because URL shorteners end up abused by scammers.

## t0reg
I then built a key-value store called [t0reg](https://reg.t0.vc). With "reg" meaning registers, it acts exactly like the t0txt pastebin but you can specify the destination ID where the data is stored. This is the most esoteric service, but the most used one because I have bots and scripts that periodically push and pull data to it. I also have bash curl aliases set up to help move data between servers with `foo | push` and `pull > foo.txt`.

## t0sig
My site's [guestbook](https://t0.vc/g/) is powered by t0sig. When a guestbook entry is submitted, it's held in memory and assigned an ID. A bot then sends me the message over Telegram and I can choose to approve it by clicking a command. After approval, the message is simply appended to the end of the guestbook page. You can read the source code [here](https://git.tannercollin.com/tanner/t0sig/src/branch/master/t0sig.py).

## t0dns
I wrote the Telegram bot [t0dns](https://t.me/t0dns_bot) to quickly manage DNS records for `dns.t0.vc`. I got tired of logging into my domain registrar every time I wanted to add a subdomain for whatever random project I was working on. Now I only have to send a command to the bot like `/add_cname sensors ms.tannercollin.com.` and it will add the `sensors.dns.t0.vc` record to a zone file that it resolves. Anyone can use it, but I routinely prune the records so don't rely on it.
