Title: Japan Photography
Date: 2023-10-19
Category: Writing
Summary: Photos from my trip to Japan.
Image: japan06lo.jpg
Nofilter: true
Tags: feed

All photos are unmodified (not even cropped) and taken with a Pixel 6a.

Click each photo for the full resolution.

Former Yasuda Garden, Tokyo:

<a href="/media/japan01hi.jpg">![[japan01lo.jpg | a bridge crosses over greenish water with vibrant trees in the background]]</a>

Shinjuku Gyoen National Garden, Tokyo:

<a href="/media/japan02hi.jpg">![[japan02lo.jpg | a tree trunk splits into a Y with ivy growing on one side]]</a>

Downtown Shinjuku, Tokyo:

<a href="/media/japan03hi.jpg">![[japan03lo.jpg | clean skyscrapers. the centre one resembles a game console.]]</a>

Shinjuku Golden Gai, Tokyo:

<a href="/media/japan04hi.jpg">![[japan04lo.jpg | a street wet from rain with bars on both sides. bar signs light up the pavement.]]</a>

Bamboo Forest, Kyoto:

<a href="/media/japan05hi.jpg">![[japan05lo.jpg | tall bamboo trees with leaves at the very top, letting some light in]]</a>

Giōji Temple, Kyoto:

<a href="/media/japan06hi.jpg">![[japan06lo.jpg | four trees with dark bark stand out against a vibrant green moss background]]</a>

Gohodo Benzaiten, Kyoto:

<a href="/media/japan07hi.jpg">![[japan07lo.jpg | a shrine is surrounded by trees, like they are consuming it in foliage]]</a>

Train to Hofu, Yamaguchi:

<a href="/media/japan08hi.jpg">![[japan08lo.jpg | train tracks taken out the front window of a train. power lines on each side.]]</a>

Ozu Island military tunnel, Yamaguchi:

<a href="/media/japan09hi.jpg">![[japan09lo.jpg | an ocean bay photo from inside a tunnel]]</a>

Ozu Island from the ferry:

<a href="/media/japan10hi.jpg">![[japan10lo.jpg | contrasting mountains in the background and a boat's wake in the foreground]]</a>

Gifu countryside:

<a href="/media/japan11hi.jpg">![[japan11lo.jpg | two hills covered in trees are reflected in the still water at the bottom]]</a>

Ueno, Tokyo:

<a href="/media/japan12hi.jpg">![[japan12lo.jpg | a restaurant under a train track. several people fill the adjacent street.]]</a>

All photos are released into the public domain / CC0 licensed.
