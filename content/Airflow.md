Title: Airflow
Date: 2023-03-07
Category: Notes
Summary: Point a fan at indoor hydroponic plants once they develop their true leaves.

Airflow across plants is important because it helps with [transpiration][1], the process of water movement through a plant and evaporation off its leaves. The water is used to move minerals up the plant from its roots. Quoting Wikipedia:

> In still air, water lost due to transpiration can accumulate in the form of vapor close to the leaf surface. This will reduce the rate of water loss, as the water potential gradient from inside to outside of the leaf is then slightly less. The wind blows away much of this water vapor near the leaf surface, making the potential gradient steeper and speeding up the diffusion of water molecules into the surrounding air. Even in wind, though, there may be some accumulation of water vapor in a thin boundary layer of slower moving air next to the leaf surface. The stronger the wind, the thinner this layer will tend to be, and the steeper the water potential gradient.

A lack of airflow can cause [mineral deficiencies][2] which is why it's recommended to point a fan at plants grown hydroponically. The plant can't do anything to fix too little airflow, but can close [stomas][3] to correct for too much airflow.


[1]: https://en.wikipedia.org/wiki/Transpiration
[2]: https://hortamericas.com/blog/science/how-to-avoid-calcium-deficiency-in-controlled-environment-food-crops/
[3]: https://en.wikipedia.org/wiki/Stoma
