Title: Protospace
Date: 2022-05-01
Category: Writing
Summary: An outline of my projects at Calgary's makerspace Protospace.
Image: protospace1.jpg
Wide: true
Tags: feed

[Protospace](https://protospace.ca) is Calgary's original makerspace, a place where people go to make things and work on projects. It's a two-bay industrial shop with a full wood working area, metal working area, electronics lab, two laser cutters, five 3D printers, and sewing room. Members pay $55/month for 24/7 access to the facility and everyone is equal: Protospace has no owners and decisions are made by the membership.

![[protospace1.jpg | both Protospace bays, metal on the left and wood on the right]]

## Do-ocracy

The driving principle behind Protospace's success is do-ocracy. If you want to make a change and it would take fewer than four hours to revert, go ahead and do it. Under a do-ocracy people are encouraged to be bold and improve the space however they want. Larger changes and disagreements are decided at the next monthly members' meeting.

Under this system, I've created several projects in order to make Protospace a better place. I'll outline them here:

### Spaceport

[[Spaceport]] is our member portal and my main project at Protospace. It tracks memberships, transactions, courses, class attendance, access cards, and statistics about Protospace and its members.

It's free and open-source software. Everyone has the right to study, change, and distribute the software and source code to anyone and for any purpose. Here's a screenshot of the home page:

![[spaceport1.png | a screenshot of the homepage of Spaceport]]

### Garden

I set up a simple hydroponics garden in a broken medical lung testing chamber that someone donated to us. Some members have a hard time turning down free junk, and it sat upstairs for about a month in the classroom filled with lung testing equipment. I eventually got tired of looking at it so one night I gutted it and threw out all the internals.

A picture of the garden is taken every 5 minutes and uploaded to Spaceport. I'll eventually make a time lapse of the vegetable growth and plot a graph of the internal air temperature.

![[protospace-garden.jpg | a side-by-side photo of a cucumber plant before and after growing huge inside a glass lung testing chamber box.]]

### Telemetry

Telemetry is a catch-all project for random sensors and displays around Protospace.

- Two air quality and temperature sensors
- A web server for querying sensor data
- Alarm armed / disarmed sensor
- A script that detects who's logged into various computers

Here's a graph of the air quality on Spaceport:

![[protospace-dust.png | two graphs comparing the dust levels in the classroom and woodshop. spikes can be seen in the woodshop graph.]]

### Airlock

Airlock is our door lock controller. Vetted Protospace members are given key cards which they scan to access the building 24/7. Airlock periodically polls a list of valid card numbers from Spaceport and checks scans against that list. If a valid card number is scanned, a relay and electric latch is opened. The card number is also reported back to Spaceport so that it can keep a log of who scanned when.

### Doorbell

We have a doorbell because members who aren't yet vetted don't get a key card and can't scan into the building 24/7. Instead they have to use the doorbell to request entry into the building. Our normal wireless doorbell's chime isn't loud enough to hear throughout the space and you can't tell which door the person is at (front or back).

I used a software-defined radio to detect our wireless doorbell's signal and play a chime over the PA system throughout the entire space. A voice then says whether it was triggered by the front door or back door.
