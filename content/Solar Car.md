Title: Solar Car
Date: 2013-04-27
Category: Creations
Summary: About my time volunteering with the University of Calgary Solar Car Team, where I designed a maximum power point tracker.
Image: solar2.jpg
Tags: feed

I joined the University of Calgary Solar Car Team in my first semester for a chance to learn things, gain practical experience, and meet people that share my interests. The car was the top Canadian team in a 3000 km race from Darwin to Adelaide, Australia in 2011. We met up at a shop on campus every Saturday morning to work on the new Generation IV of the solar car.

![[solar1.jpg | the MPPT device, a printed circuit board with bulky round electrical components held in my hand]]

## The Helianthus MPPT

I was in charge of designing and assembling the MPPTs (maximum power point trackers) for the new generation solar car. An MPPT extracts as much power out of the solar cells as possible. The solar array operates less efficiently without them. The Generation IV car, Schulich Delta (pictured below) uses seven of them: one per section of solar cells with similar lighting conditions. Andrei and I designed the MPPT above.

![[solar2.jpg | the solar car from an angle with a driver inside]]
