Title: Theatre Acoustic Panels
Date: 2021-10-21
Category: Creations
Summary: Panels for acoustic treatment in my home theatre.
Image: panel3.jpg
Tags: feed

Acoustic treatment is one of the most overlooked aspects of home audio. There's no point in spending money on premium speakers if the room they are playing in has poor acoustics.

The primary purpose of acoustic panels is to reduce the reverberations caused by sound reflecting off the smooth walls of the theatre. The path of the reflected sound is a longer distance to your ear compared to the sound coming directly from the speaker. This causes the reflected sound to be delayed by the time it reaches your ear. The delayed signal interferes with itself, causing comb filtering which distorts the signal.

Acoustic panels are placed geometrically where the sound from the speakers would reflect off the wall to reach the listener's ears. The insulation inside the panels absorbs energy from the soundwave which reduces its volume and interference.

![[ panel1.jpg | twelve L-shaped corners of the panels stacked together leaning on a table saw in a wood shop]]

I made six frames in the wood shop of my local makerspace, [[Protospace]].  After cutting the 1x4" pine boards to length, I made a jig so I could quickly join them together with screws.

![[ panel2.jpg | a panel with insulation inside on the ground about to be wrapped with black fabric]]

I added 4" batts of Rockwool insulation into each frame after it was assembled.  I then wrapped the frame with black speaker fabric and stapled it in place while trying to pull it taut.

I sat in my theatre while a friend slid a handheld mirror along the wall until I could see the middle of the speaker in its reflection. This told me the centre point of where to mount each panel because the reflected sound would take the same path to my ear.

<span class="aside">(Four in the front, two in the back)</span>

![[panel3.jpg | four black acoustic panels mounted on the walls in my home theatre]]
