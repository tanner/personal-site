Title: Grow Log, Home: Round 1
Date: 2022-07-29
Category: Notes
Summary: Grow log for my home hydroponics setup, round 1.
Wide: true

2022-02-21: Started germinating  
2022-02-24: First noticed sprouts  
2022-02-26: All sprouts are through except spinach  
2022-02-27: Put grow light on top of dome set to minimum  
2022-03-02: Checked inside. cress's roots have reached the tray below. accidentally squished front lettuces while trying to put it back. sprayed more nutrient solution on mineral wool. probably could have transplanted already. turned light off before bed, opened blinds.  
2022-03-03: Plants look very sad. connor suggested more light. put light on max and went for coffee. light ended up cooking the plants.  
2022-03-04: Plants look worse. Terminated.  

## Germination
- index is top left in tray
- rinsed all equipment with water
- shake plant food
- 1 L growth solution prepared
	  - 1.3 mL plant food
	  - 0.4 mL pH Down for 5.8 pH
- 12 mineral wool cubes each soaked for 10 seconds
- three seeds of each, except two of Spinach because they are large
- hole was opened with a broken toothpick
- seeds tamped down the hole with the toothpick
- 1 cm of fresh water added to the bottom of the tray
- remaining solution was strained and put in spray bottle

### Result
(columns left to right)

- iceberg lettuce LT541
	  - 2 cm root below mineral wool
	  - same
- arugula MS483
	  - 7x 2 cm roots
	  - 10 roots, 1x 10 cm
- romaine lettuce LT470
	  - 3x 1.5 cm roots
	  - 4 roots, 1x 6 cm
- cress MS495
	  - 2x 12 cm roots
	  - 5 roots, 1x 12 cm
- butterhead lettuce LT458
	  - 2 cm, 5 cm, 8 cm roots
	  - 4 cm, 2 cm roots
- spinach SP705
	  - never germinated
	  - never germinated
