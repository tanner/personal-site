Title: Secret Garden
Date: 2024-02-21
Category: Notes
Summary: About the hydroponics garden in my basement.

I had a "Secret Garden" in a storage room in the basement of my house. It was a [[Hydroponics | hydroponics]] system growing leafy greens and herbs. You can see an hourly photo of it below:

<a href="/media/garden_hi.jpg">![a hydroponics garden, taken from a webcam. it might be in colour or black-and-white depending on what time of day you are visiting this page. purple timestamp on the top left.](/media/garden_lo.jpg)</a>

Click the above photo for a larger version.

There's usually kale, spinach, cilantro, parsley, and green onion growing. Sometimes dill and basil. It's currently not active this fall of 2024 because I'm planning travel.

## Nutrient Film Technique

The garden uses nutrient film technique (NFT) to continuously deliver a shallow stream of nutrient solution to the plants growing in 2" ABS pipe. A submersible fountain water pump sends nutrients up 1/4" irrigation hose to each of the four pipes. The nutrients flow down the slope passed all the roots and return to the ~40 L reservoir. An air stone oxygenates the water. Two computer case fans ensure adequate [[Airflow|airflow]].

![[nft1.png | four black pipes supported horizontally by a wooden frame. eight small seedlings are growing out of the pipe under four grow lights total. two fans on the right. a pink towel is down below, covering the reservoir.]]

The nutrients are kept in a reservoir underneath the towel, which helps block light and limit [[Algae Growth|algae growth]]:

![[nft2.jpg | the reservoir with the towel removed. the yellow lid is cut in half and has a tube poking out of it with a distribution cap for the 1/4 inch irrigation lines to connect to. black return pipes are on the left, pointing down into the reservoir through a mesh bag acting as a filter.]]
## Previous Garden

My previous system [[Helios Alpha]] could grow up to six plants. It's designed around a single 102 L plastic tote. It holds enough water to harvest lettuce once before refilling. After the initial setup, the system can be ignored for weeks.

![[heliosalpha1.jpg | the hydroponics system from two angles. a black tub with yellow lid, covered in tin foil with six holes for plants. grow lights above suspended by metal shelving.]]

