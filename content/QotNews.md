Title: QotNews
Date: 2022-05-18
Category: Projects
Summary: Hacker News, Reddit, Lobsters, and Tildes articles pre-rendered in reader mode. Optimized for speed and distraction-free reading.
Image: qotnews1.jpg
Tags: feed

[QotNews](https://news.t0.vc) is a news meta-aggregator. It gathers top articles from four news aggregators: Hacker News, Reddit, Lobsters, and Tildes along with their comments. The articles are then transformed into readable versions with consistent formatting and distractions removed. All articles in the main feed are preloaded by the client so they load instantly when clicked on.

You can find the [source code](https://git.tannercollin.com/tanner/qotnews) on my Gitea.

![[qotnews1.png | screenshot of the home page with a list of stories to click on]]

I tried to make QotNews the perfect news site for me. I easily get annoyed by cookie banners and distracted by visual clutter when reading normal news articles. I especially hate auto-playing videos and "download our app" popups. All articles have consistent styling that's easy to read:

![[qotnews2.png | screenshot of an article showing the text and font used]]

It's by far my favourite project and has paid the most dividends for the amount of time I invested in programming it. I use it multiple times per day and it's become the main source of all my news. Since all the articles and comments are preloaded and saved in localStorage, it's also great for reading on airplanes.
