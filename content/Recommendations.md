Title: Recommendations
Date: 2022-06-24
Category: Writing
Summary: Software and products that I recommend you use.
Wide: true
Tags: feed

This outlines some software and devices I recommend you use: uBlock Origin, Sponsorblock, Aegis Authenticator, ThinkPad Laptops, a flashlight, a Leatherman, and various phone apps. Nothing here was sponsored.

[TOC]

## Software

### uBlock Origin

uBlock Origin is an open source ad blocker and something I install immediately on all my devices. Running an ad blocker makes browsing the web way better. It removes distracting ads (even from YouTube), invasive tracking, and makes you safer by removing potentially [fake links](https://news.t0.vc/LOBW/c#drekipus1657325184). It's the best piece of software I use even though it mostly remains unseen. And it even works on your phone.

You can install it on [Firefox Desktop](https://addons.mozilla.org/en-CA/firefox/addon/ublock-origin/), [Firefox Android](https://addons.mozilla.org/en-CA/android/addon/ublock-origin/), and [Chrome Desktop](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm?hl=en). Make sure you install the correct "Origin" version and avoid "ublock.org".

### SponsorBlock

SponsorBlock automatically skips over sponsored segments in YouTube videos. Not YouTube ads (that's what uBlock Origin is for), but the actual parts of the video sponsored by companies to advertise their products. It uses a crowd-sourced database of timestamps to seamlessly jump over those parts. It also allows you to skip to the highlight of some videos by pressing "enter" so you aren't wasting time watching exposition.

You can install it on [Firefox Desktop](https://addons.mozilla.org/en-CA/firefox/addon/sponsorblock/) and [Chrome Desktop](https://chrome.google.com/webstore/detail/sponsorblock-for-youtube/mnjggcdmjocbbbhaepdhchncahnbgone?hl=en).

### Aegis Authenticator

Aegis is a two-factor authenticator (2fa) app for Android that's free and open source. The killer feature and why I recommend it is that it supports automatic encrypted backups of the database in JSON format. You can unlock the app with a password or fingerprint. An alternative app is andOTP but there were [problems](https://news.t0.vc/EQYR/c#williamwchuang1553266688) with the backups' encryption back when I switched to Aegis.

You can install it on Android via the [Play Store](https://play.google.com/store/apps/details?id=com.beemdevelopment.aegis) or [F-Droid](https://f-droid.org/en/packages/com.beemdevelopment.aegis).

### Misc. Phone Apps

FOSS App Store: F-Droid

QR code scanner: Binary Eye

Plant identification: PlantNet

Isolate apps: Shelter

Audiobook player: Voice

Phone sensors: phyphox

Maps: Organic Maps

Photos: Immich

Calculator: andanCalc PRO

PDF viewer: Pdf Viewer Plus

NFC reader: NFC Tools



## Devices

### ThinkPad Laptops

I've had several different laptops over the years and have settled on buying ThinkPads going forward. I hate Lenovo as company because of their [Superfish scandal](https://en.wikipedia.org/wiki/Superfish#Lenovo_security_incident), but I can't deny that ThinkPads are absolutely solid. I currently own a ThinkPad X1 Carbon 6th Gen.

A lot of Linux developers use ThinkPads which means Linux is well supported on them and the drivers just work. The laptops are easy to pop open and service. Many parts are user-replaceable. My laptop charges off a small USB-C phone charger that I carry around. In a state of sickness-induced exhaustion, I spilled an entire glass of Gatorade on it and then drenched it in water to try and flush it away. I took the back off, drained it, and then pointed a fan at it for 24 hours. The laptop was working fine the next day.

### RovyVon E5 Flashlight

Small USB-C flashlight that's very bright and has a side light that's useful for camping.

<https://www.amazon.ca/dp/B0B465DBW3>

### Leatherman Skeletool CX

My preferred multitool that I everyday carry. It's light, thin, and the cutters cross over instead of pinching.

<https://www.amazon.ca/LEATHERMAN-Lightweight-Minimalist-Multi-Tool-Nightshade/dp/B0CX2HRTBP>
