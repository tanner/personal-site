Title: LED Dress
Date: 2016-03-18
Category: Creations
Summary: A dress made out of LEDs that twinkle like stars.
Image: dress1.jpg
Tags: feed

A friend of mine was attending a stars and constellations themed ball. She wanted to wear a dress that was lit up with LEDs acting as twinkling stars.  Seven of the 28 stars are aligned to resemble the Big Dipper constellation and twinkle differently than the rest, which twinkle in a random pattern.

![[dress1.jpg | a girl wearing a blue dress with a number of LEDs shining through the fabric]]

## Construction

The LEDs came from that strip that was cut up and soldered together with very small wires. Each of the LEDs can be controlled individually.

![[dress2.jpg | the controller circuit board, and the string of soldered together LEDs]]

Twenty-one of the stars are light magenta in color and twinkle by fading randomly. The seven LEDs that form the Big Dipper continually scroll through a gradient of three colors. Instead of calculating the values of each color in the gradient as the program runs, a lookup table is used.

<video autoplay="true" muted="true" loop="true" style="display:block; margin: 0 auto;">
<source src="{static}/media/dress3.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>

A video of the dress is above. Right click -> Play if needed.
