#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import os

AUTHOR = 'Tanner'
SITENAME = 'Tanner\'s Site (t0.vc)'
SITEURL = 'https://t0.vc'

PATH = 'content'

TIMEZONE = 'Canada/Mountain'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
#TAG_FEED_ATOM = 'feeds/{slug}/atom.xml'
#TAG_FEED_RSS = 'feeds/{slug}/rss.xml'
TAG_FEED_ATOM = 'test-atom.xml'
TAG_FEED_RSS = 'test-rss.xml'
RSS_FEED_SUMMARY_ONLY = False  # include full content

DEFAULT_PAGINATION = False

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.toc': {
            'toc_depth': '2-3',
            'anchorlink': True,
        },
    },
    'output_format': 'html5',
}

PLUGINS = [
    'obsidian',
    'linkclass',
]

STATIC_PATHS = ['media', 'extra', 'text']

EXTRA_PATH_METADATA = {
    #'extra/favicon.svg': {'path': 'favicon.svg'},
}

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = 'themes/lite'

# turn off useless outputs
TAG_SAVE_AS = ''
CATEGORY_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
ARCHIVES_SAVE_AS = ''
AUTHORS_SAVE_AS = ''
CATEGORIES_SAVE_AS = ''
TAGS_SAVE_AS = ''

INDEX_SAVE_AS = 'index.html'
ARTICLE_URL = '{slug}'
ARTICLE_SAVE_AS = '{slug}/index.html'
PAGE_URL = '{slug}'
PAGE_SAVE_AS = '{slug}/index.html'

def list_text_files():
    return sorted(os.listdir('./content/text'))

JINJA_GLOBALS = {'list_text_files': list_text_files}

PROD = True
